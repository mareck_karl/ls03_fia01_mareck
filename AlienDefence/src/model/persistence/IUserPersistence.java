package model.persistence;

import model.persistenceDummy.User;

public interface IUserPersistence {

	User readUser(String username);

}